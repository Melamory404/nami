package com.enchantme.nami.system;

/*
Some constants in the project.
 */
public final class Constants {

    private static Double healthKitValue = 25d;
    private static Double shieldKitValue = 25d;
    private static Double scoreKitValue = 100d;

    public static void setHealthKitValue(Double value) {
        healthKitValue = value;
    }
    public static Double getHealthKitValue() {
        return healthKitValue;
    }
    public static void setShieldKitValue(Double value) {
        shieldKitValue = value;
    }
    public static Double getShieldKitValue() {
        return shieldKitValue;
    }
    public static void setScoreKitValue(Double value) {
        scoreKitValue = value;
    }
    public static Double getScoreKitValue() {
        return scoreKitValue;
    }

}
