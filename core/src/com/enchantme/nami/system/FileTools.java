package com.enchantme.nami.system;

import java.io.*;

/*
Final class that contains methods to work with file system.
 */
public final class FileTools {

    public static void createDirectory(String directoryPath) throws IOException {
        File dir = new File(directoryPath);
        if (dir.exists()) {
            return;
        }
        if (dir.mkdirs()) {
            return;
        }
        throw new IOException("Failed to create directory '" + dir.getAbsolutePath() + "' for an unknown reason.");
    }

    public static void createFile(String filePath) throws IOException {
        File file = new File(filePath);

        if(file.exists()) {
            return;
        }
        if(file.createNewFile()){
            return;
        }
        throw new IOException("Failed to create file '" + file.getAbsolutePath() + "' for an unknown reason.");
    }

    public static String readFileAsString(String filePath) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line = br.readLine();
            while(line != null) {
                sb.append(line);
                line = br.readLine();
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
