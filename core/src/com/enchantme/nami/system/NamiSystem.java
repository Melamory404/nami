package com.enchantme.nami.system;

/*
Singleton that used to obtain config and profile managers.
 */
public class NamiSystem {

    private static ConfigManager configManager;
    private static ProfileManager profileManager;

    public static ConfigManager getConfigManager() {
        if(configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }

    public static ProfileManager getProfileManager() {
        if(profileManager == null) {
            profileManager = new ProfileManager();
        }
        return profileManager;
    }

    public static void dispose() {
        profileManager.dispose();
    }
}
