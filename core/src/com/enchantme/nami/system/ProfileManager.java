package com.enchantme.nami.system;

import com.badlogic.gdx.utils.Disposable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ProfileManager implements Disposable {

    private static final String profileDirectory = "Profile";
    private static String profileFile;

    private Map<String, Object> stats;

    private Gson gson;
    private static final Type hashType = new TypeToken<HashMap<String, Object>>(){}.getType();

    public ProfileManager() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        profileFile = profileDirectory + "/" + "profile.json";
        try {
            FileTools.createDirectory(profileDirectory);
            FileTools.createFile(profileFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jsonProfile  = FileTools.readFileAsString(profileFile);
        stats = gson.fromJson(jsonProfile, hashType);

        if(stats == null) {
            stats = new HashMap<String, Object>();
        }

        if (stats.isEmpty()) {
            stats.put("MaxScore", 0d);
            dispose();
        }
        if(!stats.containsKey("MaxScore")) {
            stats.put("MaxScore", 0d);
        }
        stats.put("CurrentScore", 0d);
    }

    public Double getScore() {
        return (Double)stats.get("CurrentScore");
    }

    public void setScore(Double score) {
        stats.put("CurrentScore", score);
      //  dispose();
    }

    public Double getMaxScore() {
        return (Double)stats.get("MaxScore");
    }

    public Double getCurrentScore() {
        return (Double) stats.get("CurrentSaveScore");
    }

    public void setCurrentSaveScore() {
        Double currentSaveScore = ((Double) stats.get("CurrentScore"));
        if (currentSaveScore == null) {
            stats.put("CurrentSaveScore", 0d);
        } else {
            stats.put("CurrentSaveScore", currentSaveScore);
        }
    }

    private void setMaxScore() {
        Double maxScore = ((Double)stats.get("MaxScore"));
        Double currentScore = ((Double)stats.get("CurrentScore"));
        if(currentScore == null) {
            stats.put("CurrentScore",0d);
            currentScore = 0d;
        }
        else if (maxScore == null) {
            stats.put("MaxScore", 0d);
            maxScore = 0d;
        }
        if(currentScore > maxScore) {
            maxScore = currentScore;
            currentScore = 0d;
            stats.put("CurrentScore", currentScore);
            stats.put("MaxScore", maxScore);
        }
    }

    @Override
    public void dispose() {
        try {
            setMaxScore();
            stats.put("CurrentScore",0d);
            PrintWriter writer = new PrintWriter(profileFile);
            writer.print(gson.toJson(stats, hashType));
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
