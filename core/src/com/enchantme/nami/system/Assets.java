package com.enchantme.nami.system;

/*
Assets enum, should be used to obtain sprites path's.
 */
public enum Assets {
    PLAYER_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Player/Player.png";
        }
    },
    PLAYER_BULLET_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Player/PlayerBullet.png";
        }
    },
    LINEAR_SHIP_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/LinearShip/LinearShip.png";
        }
    },
    RAM_SHIP_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/RamShip/RamShip.png";
        }
    },
    MINE_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Mine/Mine.png";
        }
    },
    LINEAR_SHIP_BULLET_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/LinearShip/LinearShipBullet.png";
        }
    },
    HEALTH_KIT_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Kits/HealthKit.png";
        }
    },
    SHIELD_KIT_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Kits/ShieldKit.png";
        }
    },
    SCORE_KIT_SPRITE {
        @Override
        public String getAsset() {
            return "GameObjects/Kits/ScoreKit.png";
        }
    };
    public abstract String getAsset();
}
