package com.enchantme.nami;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.enchantme.nami.screens.ScreenEnum;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.system.NamiSystem;

public class Nami extends Game {

	public static final int PPM = 100;

	@Override
	public void create () {
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
        NamiSystem.getProfileManager().dispose();
	}
}
