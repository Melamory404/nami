package com.enchantme.nami.screens;

import com.enchantme.nami.screens.basic.GameModeSelectScreen;
import com.enchantme.nami.screens.basic.MainMenuScreen;
import com.enchantme.nami.screens.basic.SettingsScreen;
import com.enchantme.nami.screens.survival_mode.SurvivalModeEndScreen;
import com.enchantme.nami.screens.survival_mode.SurvivalModeGameScreen;

public enum ScreenEnum {

    MAIN_MENU {
        public AbstractScreen getScreen(Object... params) {
            return new MainMenuScreen();
        }
    },

    SETTINGS {
      public  AbstractScreen getScreen(Object... params) { return new SettingsScreen(); }
    },

    GAME_MODE_SELECT {
        public AbstractScreen getScreen(Object... params) {
            return new GameModeSelectScreen();
        }
    },

    SURVIVAL_MODE_GAME {
      public AbstractScreen getScreen(Object... params) {
          return new SurvivalModeGameScreen();
      }
    },

    SURVIVAL_MODE_END {
        public AbstractScreen getScreen (Object... params) {
            return new SurvivalModeEndScreen();
        }
    };

    public abstract AbstractScreen getScreen(Object... params);
}
