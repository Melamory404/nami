package com.enchantme.nami.screens.basic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.enchantme.nami.screens.AbstractScreen;
import com.enchantme.nami.screens.ScreenEnum;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.system.NamiSystem;

public class MainMenuScreen extends AbstractScreen {

    private Table table;
    private TextButton startButton, settingsButton, exitButton;
    private Label maxScoreLabel;
    private Skin menuSkin;

    public MainMenuScreen() {
        super(1280,960);
        menuSkin = new Skin(Gdx.files.internal("UI/MainMenu/menuSkin.json"));
        //    NamiSystem.getProfileManager().dispose();
    }

    @Override
    public void buildStage() {

        Sprite backgroundSprite = new Sprite(new Texture("GameObjects/Background/background.png"));

        table = new Table(menuSkin);
        table.setBounds(0,0, 1280, 960);

        maxScoreLabel = new Label(String.format("Record: %s", NamiSystem.getProfileManager().getMaxScore())
                ,menuSkin);

        settingsButton = new TextButton("Settings",menuSkin);

        settingsButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                ScreenManager.getInstance().showScreen(ScreenEnum.SETTINGS);
            }
        });

        exitButton = new TextButton("Exit",menuSkin);
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        //TODO: Change to GameModeSelectScreen in future.

        startButton = new TextButton("Start",menuSkin);
        startButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                ScreenManager.getInstance().showScreen(ScreenEnum.SURVIVAL_MODE_GAME);
            }
        });

        startButton.padLeft(-800);
        startButton.padBottom(300);

        settingsButton.padLeft(-800);
        settingsButton.padBottom(100);

        exitButton.padLeft(-800);
        exitButton.padBottom(-100);

        table.add(startButton);
        table.add(settingsButton);
        table.add(exitButton);
        table.add(maxScoreLabel);
        SpriteDrawable background = new SpriteDrawable(backgroundSprite);
        table.pack();
        table.setBackground(background);
        //table.debug();
        addActor(table);
    }

    @Override
    public void resize(int width, int height) {

        getViewport().update(width,height,true);

        table.setFillParent(true);
        table.setTransform(true);
        table.setSize(width,height);
    }

    @Override
    public void dispose() {
        menuSkin.dispose();
    }
}
