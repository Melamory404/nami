package com.enchantme.nami.screens.basic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.enchantme.nami.screens.AbstractScreen;

public class SettingsScreen extends AbstractScreen {

    private Skin menuSkin;

    private Table table;
    private Label maxScoreLabel;

    public SettingsScreen() {
        super(1280,960);
        menuSkin = new Skin(Gdx.files.internal("UI/MainMenu/menuSkin.json"));
        //    NamiSystem.getProfileManager().dispose();
    }

    @Override
    public void buildStage() {
        table = new Table(menuSkin);
        table.setBounds(0,0, 1280, 960);

        maxScoreLabel = new Label(String.format("Record: %s", "344")
                ,menuSkin);

        table.add(maxScoreLabel);
        addActor(table);
    }

}
