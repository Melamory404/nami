package com.enchantme.nami.screens.survival_mode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.enchantme.nami.screens.AbstractScreen;
import com.enchantme.nami.screens.ScreenEnum;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.system.NamiSystem;

public class SurvivalModeEndScreen extends AbstractScreen {

    private Table table;
    private TextButton restartButton, mainMenuButton;
    private Label currentScoreLabel, maxScoreLabel;
    private Skin menuSkin;

    public SurvivalModeEndScreen() {
        super(1280, 960);
        menuSkin = new Skin(Gdx.files.internal("UI/MainMenu/menuSkin.json"));
    }

    @Override
    public void buildStage() {
        table = new Table(menuSkin);
        table.setBounds(0, 0, 1280, 960);

        maxScoreLabel = new Label(String.format("Record: %s", NamiSystem.getProfileManager().getMaxScore()), menuSkin);

        currentScoreLabel = new Label(String.format("Record: %s", NamiSystem.getProfileManager().getCurrentScore()), menuSkin);

        restartButton = new TextButton("Restart", menuSkin);

        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ScreenManager.getInstance().showScreen(ScreenEnum.SURVIVAL_MODE_GAME);
            }
        });

        mainMenuButton = new TextButton("Main menu", menuSkin);

        mainMenuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU);
            }
        });

        table.add(maxScoreLabel).padLeft(600).padBottom(75);
        table.row();
        table.add(currentScoreLabel).padLeft(600).padBottom(75);
        table.row();
        table.add(restartButton).padLeft(550);
        table.add(mainMenuButton).padRight(550);
        table.debug();
        addActor(table);
    }

}
