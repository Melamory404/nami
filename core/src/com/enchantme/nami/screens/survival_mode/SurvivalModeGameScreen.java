package com.enchantme.nami.screens.survival_mode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_mode.ScoreEnum;
import com.enchantme.nami.game_mode.survival_mode.SurvivalModeContactListener;
import com.enchantme.nami.game_mode.survival_mode.SurvivalModeGenerator;
import com.enchantme.nami.game_mode.survival_mode.SurvivalModeInputManager;
import com.enchantme.nami.screens.AbstractScreen;
import com.enchantme.nami.screens.GameStates;
import com.enchantme.nami.screens.ScreenEnum;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.system.NamiSystem;

public class SurvivalModeGameScreen extends AbstractScreen {

    private World world;
    private Box2DDebugRenderer b2d;
    private SurvivalModeGenerator survivalModeGenerator;

    private Stage pauseStage;
    private Table table, pauseTable;
    private Label scoreLabel, healthLabel, shieldLabel;
    private TextButton resumePauseButton, exitPauseButton;
    private Skin menuSkin;

    private SurvivalModeInputManager survivalModeInputManager;

    private GameStates state = GameStates.RUN;

    private Sprite backgroundSprite;

    private Double score;

    public SurvivalModeGameScreen() {
        super(1280/Nami.PPM,960/Nami.PPM);
        menuSkin = new Skin(Gdx.files.internal("UI/SurvivalMode/survivalModeSkin.json"));
        world = new World(new Vector2(0,0),true);
        b2d = new Box2DDebugRenderer();
        survivalModeGenerator = new SurvivalModeGenerator(this);
        world.setContactListener(new SurvivalModeContactListener(survivalModeGenerator, this));
        configurePauseStage();
    }

    private void configurePauseStage() {
        pauseStage = new Stage(new FillViewport(1280.0f,960.0f,new OrthographicCamera()));

        pauseTable = new Table(menuSkin);
        pauseTable.setBounds(1, 1, 1280,960 );

        resumePauseButton = new TextButton("Resume",menuSkin);

        resumePauseButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pauseScreen();
            }
        });

        exitPauseButton = new TextButton("Exit",menuSkin);

        exitPauseButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showScreen(ScreenEnum.MAIN_MENU);
            }
        });


        pauseTable.add(resumePauseButton);
        pauseTable.row();
        pauseTable.row();
        pauseTable.add(exitPauseButton);
        pauseTable.debug();
        pauseStage.addActor(pauseTable);
    }

    @Override
    public void buildStage() {

        Image background = new Image(new Texture("GameObjects/Background/background.png"));
        background.setZIndex(0);
        background.setBounds(0,0,1920,1080);
        background.setScale(1 /(float) Nami.PPM);
        table = new Table(menuSkin);
        table.setZIndex(1);
    //    table.top();
        table.setBounds(getWidth() / 2, getHeight() / 2, 1920, 1080);
        //table.setFillParent(true);

        scoreLabel = new Label("wtf",menuSkin);
        score = NamiSystem.getProfileManager().getScore();
        scoreLabel.setText(String.format("Score %s", score));

        healthLabel = new Label("Health: ", menuSkin);
        healthLabel.setText(String.format("Health: %s", survivalModeGenerator.getPlayer().getHealth() ));

        shieldLabel = new Label("Shield: ", menuSkin);
        shieldLabel.setText(String.format("Shield: %s", survivalModeGenerator.getPlayer().getShield() ));

        table.setScale((float)1/Nami.PPM);
        table.add(scoreLabel).expandX().padTop(-5.5F * Nami.PPM).padLeft(9.5f * Nami.PPM);
        table.add(healthLabel).expandX().padTop(-5.5F * Nami.PPM).padRight(9.5f * Nami.PPM);
        table.add(shieldLabel).expandX().padTop(-4.5F * Nami.PPM).padRight(9.5f * Nami.PPM);

        table.debug();
        addActor(background);
        addActor(table);
      //  addActor(pauseTable);
    }

    public void changeScoreLabel(Double score, ScoreEnum scoreEnum) {
        switch (scoreEnum) {
            case SET:
                this.score = score;
                break;
            case PLUS:
                this.score += score;
                break;
            case ERASE:
                this.score = 0d;
                break;
            case MINUS:
                this.score -= score;
                break;
        }
        scoreLabel.setText(String.format("Score %s", this.score ));
        NamiSystem.getProfileManager().setScore(this.score);
    }

    @Override
    public void render(float delta) {
        switch (state) {
            case RUN:
                super.render(delta);
                update(delta);
                Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                act(delta);
                draw();
                batch.setProjectionMatrix(camera.combined);
             //   b2d.render(world,camera.combined);
                batch.setProjectionMatrix(camera.combined);
                batch.begin();
                survivalModeGenerator.render(batch);
                batch.end();
                break;
            case PAUSE:
                pauseStage.act(delta);
                pauseStage.draw();
                break;
        }
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width,height,true);

        table.setFillParent(true);
        table.setTransform(true);
        table.setSize(width,height);
    }

    @Override
    public void update(float dt) {
        super.update(dt);
        world.step(dt,14,2);
        survivalModeGenerator.update(dt);
    }

    @Override
    public void show() {
        survivalModeInputManager = new SurvivalModeInputManager(survivalModeGenerator,this);
        Gdx.input.setInputProcessor(survivalModeInputManager);
    }

    @Override
    public void dispose() {
        super.dispose();
        //batch.dispose();
        //TODO: batch.dispose() крашит прилогу втф
        menuSkin.dispose();
      //  survivalModeGenerator.dispose();
        NamiSystem.dispose();
    }

    public Double getScore() {
        return score;
    }

    public void setHealthLabel(Double health) {
        healthLabel.setText(String.format("Health: %s", health ));
    }

    public void setShieldLabel(Double shield) {
        shieldLabel.setText(String.format("Shield: %s", shield ));
    }


    public World getWorld() {
        return world;
    }

    @Override
    public void hide() {
        super.hide();
    }


    public void pauseScreen() {
        if(state == GameStates.PAUSE) {
            state = GameStates.RUN;
            Gdx.input.setInputProcessor(survivalModeInputManager);
            table.setVisible(true);
            pauseTable.setVisible(false);
        }
        else {
            state = GameStates.PAUSE;
            Gdx.input.setInputProcessor(pauseStage);
            table.setVisible(false);
            pauseTable.setVisible(true);
        }
    }
}
