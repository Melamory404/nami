package com.enchantme.nami.screens;

import com.badlogic.gdx.Screen;
import com.enchantme.nami.Nami;
import com.enchantme.nami.screens.survival_mode.SurvivalModeGameScreen;

public class ScreenManager {

    private static ScreenManager instance;

    private Nami game;

    private ScreenManager() {
        super();
    }

    public static ScreenManager getInstance() {
        if (instance == null) {
            instance = new ScreenManager();
        }
        return  instance;
    }

    public void initialize(Nami game) {
        this.game = game;
    }

    public void showScreen(ScreenEnum screenEnum, Object... params) {

        Screen currentScreen = game.getScreen();

        AbstractScreen newScreen = screenEnum.getScreen(params);
        newScreen.buildStage();
        game.setScreen(newScreen);

        if (currentScreen != null) {
            currentScreen.dispose();
        }
    }

    public Nami getGame() {
        return game;
    }
}
