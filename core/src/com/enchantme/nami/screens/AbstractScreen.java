package com.enchantme.nami.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.enchantme.nami.Nami;

public abstract class AbstractScreen extends Stage implements Screen {

    public SpriteBatch batch;
    public OrthographicCamera camera;

    ShaderProgram shader;

    protected AbstractScreen() {
        super (new FillViewport(1280.0f/ Nami.PPM,960.0f/Nami.PPM,new OrthographicCamera()));
        batch = new SpriteBatch();
        camera = (OrthographicCamera) getViewport().getCamera();
        camera.setToOrtho(false);
    }

    protected AbstractScreen(int worldWidth, int worldHeight) {
        super (new FillViewport(worldWidth,worldHeight,new OrthographicCamera()));
        ShaderProgram.pedantic = false;
        shader = new ShaderProgram(Gdx.files.internal("Shaders/default.vert"),Gdx.files.internal("Shaders/default.frag"));
        if(!shader.isCompiled()) {
            System.err.println(shader.getLog());
            System.exit(0);
        }

        batch = new SpriteBatch();
        batch.setShader(shader);
        camera = (OrthographicCamera) getViewport().getCamera();
        camera.setToOrtho(false);
    }

    public abstract void buildStage();

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.act(delta);
        super.draw();
    }

    public  void update(float dt) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

}
