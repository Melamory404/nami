package com.enchantme.nami.game_objects.enemy_mine;

import com.enchantme.nami.game_objects.basic.HealthSystem;

public class MineHealth extends HealthSystem {
    public MineHealth() {
        super(5d, 0d);
    }

    @Override
    public void getDamage(Double damage) {
        super.getDamage(damage);
    }
}
