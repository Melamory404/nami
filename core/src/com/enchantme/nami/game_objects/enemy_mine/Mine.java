package com.enchantme.nami.game_objects.enemy_mine;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.Enemy;
import com.enchantme.nami.system.Assets;

public class Mine extends Enemy {

    private World world;
    private MineStates state;
    private MineHealth mineHealth;
    private Double mineDamage = 2d;

    private Double deathTimeLimit = 4d;
    private Double deathTime = 0d;

    private Body body;

    private static final float MINE_RESIZE_FACTOR = 1.5f;

    public Mine(World world, Vector2 startPosition) {
        super(Assets.MINE_SPRITE.getAsset(), startPosition);
        this.world = world;
        setSize(getWidth() / (Nami.PPM * MINE_RESIZE_FACTOR),
                getHeight() / (Nami.PPM * MINE_RESIZE_FACTOR));
        mineHealth = new MineHealth();
        changeState(MineStates.DISACTIVATED);
        define();
    }

    private void changeState(MineStates state) {
        this.state = state;
    }

    @Override
    public boolean isDead() {
        return killed;
    }

    @Override
    public void attack(float delta) {

    }

    @Override
    public boolean allowToRemoveFromWorld() {
        return destroyed;
    }

    @Override
    public void move() {
        currentPosition.x = body.getPosition().x - getWidth() / 2;
        currentPosition.y = body.getPosition().y - getHeight() / 2;
        setPosition(currentPosition.x, currentPosition.y);
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x / Nami.PPM, startPosition.y / Nami.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 2), (getHeight() / 2));

        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);

    }

    @Override
    public void render(SpriteBatch batch) {
        if (!killed) {
            super.draw(batch);
        }
    }

    @Override
    public void update(float dt) {
        if (killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
        } else {
            move();
            deathTime += dt;
        }
        if (deathTime >= deathTimeLimit) {
            kill();
        }
    }

    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public void getDamage(Double damage) {
        mineHealth.getDamage(damage);
        if(mineHealth.getHealth() <= 0d) {
            kill();
        }
    }

    @Override
    public Double getCollisionDamage() {
        return mineDamage;
    }
}
