package com.enchantme.nami.game_objects.enemy_ram_ship;

import com.enchantme.nami.game_objects.basic.HealthSystem;

public class RamShipHealth extends HealthSystem {

    public RamShipHealth() {
        super(15d, 0d);
    }

    @Override
    public void getDamage(Double damage) {
        super.getDamage(damage);
    }

}
