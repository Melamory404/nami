package com.enchantme.nami.game_objects.enemy_ram_ship;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.Enemy;
import com.enchantme.nami.system.Assets;

public class RamShip extends Enemy {

    //TODO: Уменьшить размер спрайта в 2 раза

    private static final float RAM_SHIP_RESIZE_FACTOR = 2.0f;
    RamShipHealth ramShipHealth;
    private Body body;
    private World world;
    private Double ramDamage = 10d;

    public RamShip(World world, Vector2 startPosition) {
        super(Assets.RAM_SHIP_SPRITE.getAsset(), startPosition);
        this.world = world;
        flip(false,true);
        setSize(getWidth() / (Nami.PPM * RAM_SHIP_RESIZE_FACTOR),
                getHeight() / (Nami.PPM * RAM_SHIP_RESIZE_FACTOR));

        ramShipHealth = new RamShipHealth();
        define();
    }

    @Override
    public boolean isDead() {
        return killed;
    }

    @Override
    public void attack(float delta) {

    }

    @Override
    public boolean allowToRemoveFromWorld() {
        return destroyed;
    }

    @Override
    public void move() {
        currentPosition.x = body.getPosition().x - getWidth() / 2;
        currentPosition.y = body.getPosition().y - getHeight() / 2;
        setPosition(currentPosition.x, currentPosition.y);
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x / Nami.PPM, startPosition.y / Nami.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();

        velocity = new Vector2(0, 2.0f);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(getWidth() / 2, getHeight() / 2);
        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);
        body.setLinearVelocity(new Vector2(0, -velocity.y));
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!killed) {
            super.draw(batch);
        }
    }

    @Override
    public void update(float dt) {
        if (killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
        } else {
            move();
        }
        if (currentPosition.y < -1) {
            kill();
        }

    }

    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public Double getCollisionDamage() {
        return ramDamage;
    }

    @Override
    public void getDamage(Double damage) {
        ramShipHealth.getDamage(damage);
        if (ramShipHealth.getHealth() <= 0d) {
            kill();
        }
    }
}
