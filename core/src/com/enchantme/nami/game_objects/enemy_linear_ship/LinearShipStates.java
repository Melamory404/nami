package com.enchantme.nami.game_objects.enemy_linear_ship;

public enum LinearShipStates {
    MOVE_UP,
    MOVE_DOWN,
    MOVE_LEFT,
    MOVE_RIGHT,
    STAY
}
