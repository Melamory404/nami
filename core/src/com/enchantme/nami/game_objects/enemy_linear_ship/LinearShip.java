package com.enchantme.nami.game_objects.enemy_linear_ship;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.Enemy;
import com.enchantme.nami.system.Assets;

import java.util.ArrayList;

/*
Enemy ship that moves in 4 directions and shoots some bullets that can cause damage to player.
 */
public class LinearShip extends Enemy {

    private LinearShipHealth linearShipHealth;
    private LinearShipStates state;
    private World world;
    private Body body;

    private static final float maxVelocity = 1f;
    private static final float damping = 2f;
    private static final float LINEAR_SHIP_RESIZE_FACTOR = 2f;

    private static final float shotDelay = 1.8f;
    private float timeToDelay = 0.0f;
    private boolean allowToShoot = true;

    private ArrayList<LinearShipBullet> bullets;
    private ArrayList<LinearShipBullet> bulletsToRemove;

    public LinearShip(World world, Vector2 startPosition) {
        super(Assets.LINEAR_SHIP_SPRITE.getAsset(), startPosition);
        linearShipHealth = new LinearShipHealth();
        this.world = world;
        setSize(getWidth() / (Nami.PPM * LINEAR_SHIP_RESIZE_FACTOR),
                getHeight() / (Nami.PPM * LINEAR_SHIP_RESIZE_FACTOR));
        define();
        changeState(LinearShipStates.MOVE_DOWN);
        bullets = new ArrayList<LinearShipBullet>();
        bulletsToRemove = new ArrayList<LinearShipBullet>();
    }

    public LinearShip(World world, Vector2 startPosition, LinearShipStates state) {
        this(world, startPosition);
        changeState(state);
    }

    private void changeState(LinearShipStates state) {
        this.state = state;
        switch (this.state) {
            case STAY:
                body.setLinearVelocity(new Vector2(0, 0));
                flip(false, true);
                break;
            case MOVE_UP:
                body.setLinearVelocity(new Vector2(0, maxVelocity));
                flip(false, false);
                break;
            case MOVE_DOWN:
                body.setLinearVelocity(new Vector2(0, -maxVelocity));
                flip(false, true);
                break;
            case MOVE_LEFT:
                body.setLinearVelocity(new Vector2(-maxVelocity, 0));
                break;
            case MOVE_RIGHT:
                body.setLinearVelocity(new Vector2(maxVelocity, 0));
                break;
        }
    }

    @Override
    public void move() {
        currentPosition.x = body.getPosition().x - getWidth() / 2;
        currentPosition.y = body.getPosition().y - getHeight() / 2;
        setPosition(currentPosition.x, currentPosition.y);
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!killed) {
            super.draw(batch);
        }
        for (LinearShipBullet bullet : bullets) {
            if(!bullet.isDead()) {
                bullet.render(batch);
            }
        }
    }

    @Override
    public void update(float delta) {
        if (killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
        } else {
            move();
        }
        if (currentPosition.y < -1) {
            kill();
        }

        attack(delta);

    }

    @Override
    public void attack(float delta) {
        if (allowToShoot && !killed) {
            bullets.add(new LinearShipBullet(world, this));
            allowToShoot = false;
        }

        for (LinearShipBullet bullet : bullets) {
                bullet.update(delta);
        }

        if (!bulletsToRemove.isEmpty()) {
            for (LinearShipBullet bullet : bulletsToRemove) {
                bullets.remove(bullet);
            }
            bulletsToRemove.clear();
        }
        timeToDelay += delta;
        if (timeToDelay >= shotDelay) {
            timeToDelay = 0.0f;
            allowToShoot = true;
        }
    }

    @Override
    public boolean allowToRemoveFromWorld() {
        return bullets.size() == 0 && bulletsToRemove.size() == 0;
    }

    @Override
    public void kill() {
        killed = true;
    }


    @Override
    public void getDamage(Double damage) {
        linearShipHealth.getDamage(damage);
        if (linearShipHealth.getHealth() <= 0d) {
            kill();
        }
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x / Nami.PPM, startPosition.y / Nami.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 2), (getHeight() / 2));

        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public boolean isDead() {
        return killed;
    }

    public void removeBullet(LinearShipBullet bullet) {
        bulletsToRemove.add(bullet);
    }

    public float getResizeFactor() {
        return LINEAR_SHIP_RESIZE_FACTOR;
    }

    public Vector2 getCurrentPosition() {
        return currentPosition;
    }

}
