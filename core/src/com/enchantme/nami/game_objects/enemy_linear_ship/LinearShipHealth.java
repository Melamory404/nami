package com.enchantme.nami.game_objects.enemy_linear_ship;

import com.enchantme.nami.game_objects.basic.HealthSystem;

public class LinearShipHealth extends HealthSystem {

    public LinearShipHealth() {
        super(10d,0d);
    }

    @Override
    public void getDamage(Double damage) {
        super.getDamage(damage);
    }
}
