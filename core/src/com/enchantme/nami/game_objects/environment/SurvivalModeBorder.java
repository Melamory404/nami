package com.enchantme.nami.game_objects.environment;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.enchantme.nami.game_objects.basic.GameObject;

import java.util.ArrayList;

public class SurvivalModeBorder extends GameObject {

    private World world;
    private Body body;

    private ArrayList<Border> borders;
    private ArrayList<PolygonShape> borderShapes;
    private ArrayList<Vector2> borderPositions;

    public SurvivalModeBorder(World world) {
        this.world = world;
        borders = new ArrayList<Border>();
        initBorderSizes();
        define();
    }

    private void initBorderSizes() {
        borderShapes = new ArrayList<PolygonShape>();
        borderPositions = new ArrayList<Vector2>();

        PolygonShape shape = new PolygonShape();

        shape.setAsBox(6f,0.25f);
        borderShapes.add(shape);
        borderPositions.add(new Vector2(600, 20));

        shape = new PolygonShape();
        shape.setAsBox(6f,0.25f);
        borderShapes.add(shape);
        borderPositions.add(new Vector2(600, 820));

        shape = new PolygonShape();
        shape.setAsBox(0.25f,6f);
        borderShapes.add(shape);
        borderPositions.add(new Vector2(-20,500));

        shape = new PolygonShape();
        shape.setAsBox(0.25f,6f);
        borderShapes.add(shape);
        borderPositions.add(new Vector2(1220,500));
    }

    @Override
    public void render(SpriteBatch batch) {
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void kill() {

    }

    @Override
    public void define() {
        for (int i = 0; i < 4; ++i) {
            borders.add(new Border(world, borderShapes.get(i), borderPositions.get(i)));
        }
    }

    @Override
    public void getDamage(Double damage) {

    }
}
