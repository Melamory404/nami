package com.enchantme.nami.game_objects.environment;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.GameObject;

public class Border extends GameObject {

    private World world;
    private PolygonShape shape;
    private Body body;
    private Vector2 startPosition;

    private boolean killed = false;
    private boolean destroyed = false;

    public Border(World world, PolygonShape shape, Vector2 startPosition) {
        this.world = world;
        this.shape = shape;
        this.startPosition = startPosition;
        define();
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!killed) {
            super.draw(batch);
        }
    }

    @Override
    public void update(float dt) {
        if (killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
        }
    }

    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x / Nami.PPM, startPosition.y / Nami.PPM);
        bodyDef.type = BodyDef.BodyType.StaticBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;

        body.createFixture(fixtureDef).setUserData(this);

    }

    @Override
    public void getDamage(Double damage) {

    }
}
