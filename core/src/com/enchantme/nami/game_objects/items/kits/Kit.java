package com.enchantme.nami.game_objects.items.kits;

import com.enchantme.nami.game_objects.basic.MovableObject;

public abstract class Kit extends MovableObject {
    public abstract Double getBonus();

    public Kit() {
        super();
    }

    Kit(String spriteName) {
        super(spriteName);
    }

    public abstract boolean allowToRemoveFromWorld();
}
