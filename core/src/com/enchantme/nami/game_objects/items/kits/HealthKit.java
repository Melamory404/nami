package com.enchantme.nami.game_objects.items.kits;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.system.Assets;
import com.enchantme.nami.system.Constants;

public class HealthKit extends Kit {

    private Double healthBonus;

    private World world;
    private Body body;
    private Vector2 startPosition;
    private Vector2 currentPosition;
    private KitStates state = KitStates.STAY;

    private boolean killed = false;
    private boolean destroyed = false;
    private float currentTime = 0f;
    private static final float timeLimit = 5f;

    private static final float HEALTH_KIT_RESIZE_FACTOR = 1.0f;

    public HealthKit(World world, Vector2 startPosition) {
        super(Assets.HEALTH_KIT_SPRITE.getAsset());
        this.world = world;
        this.startPosition = startPosition;
        currentPosition = startPosition;
        setSize(getWidth() / (Nami.PPM * HEALTH_KIT_RESIZE_FACTOR),
                getHeight() / (Nami.PPM * HEALTH_KIT_RESIZE_FACTOR));
        healthBonus = Constants.getHealthKitValue();
        velocity = new Vector2(1,1);
        define();
    }

    @Override
    public Double getBonus() {
        return healthBonus;
    }

    @Override
    public boolean allowToRemoveFromWorld() {
        return destroyed;
    }

    @Override
    public void move() {
        currentPosition.x = body.getPosition().x - getWidth() / 2;
        currentPosition.y = body.getPosition().y - getHeight() / 2;
        setPosition(currentPosition.x, currentPosition.y);
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x / Nami.PPM, startPosition.y / Nami.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth() / 2), (getHeight() / 2));

        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!killed) {
            super.draw(batch);
        }
    }

    @Override
    public void update(float dt) {
        if (killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
        }
        else {
            updateState(state);
            move();
        }
        currentTime += dt;

        if(currentTime >= timeLimit) {
            killed = true;
        }
    }

    public void changeState(KitStates state) {
        this.state = state;
    }

    private void updateState(KitStates state) {
        switch (state){
            case MOVE_RIGHT:
                body.setLinearVelocity(velocity.x,0);
                break;
            case MOVE_LEFT:
                body.setLinearVelocity(-velocity.x,0);
                break;
            case MOVE_DOWN:
                body.setLinearVelocity(0,-velocity.y);
                break;
            case MOVE_UP:
                body.setLinearVelocity(0,velocity.y);
                break;
            case STAY:
                body.setLinearVelocity(0,0);
                break;
        }
    }

    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public void getDamage(Double damage) {
        System.out.println("No needed");
    }
}
