package com.enchantme.nami.game_objects.items.kits;

public enum KitStates {
    MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_UP,
    MOVE_DOWN,
    STAY
}
