package com.enchantme.nami.game_objects.basic;

/*
All classes that have some health or shield should have field with class that extends this one.
 */
public abstract class HealthSystem implements Damageable {
    private Double health;
    private Double shield;

    public HealthSystem() {
        this(10d,10d);
    }

    public HealthSystem(Double health, Double shield) {
        this.health = health;
        this.shield = shield;
    }

    private void setHealth(Double health) {
        this.health = health;
    }

    private void setShield(Double shield) {
        this.shield = shield;
    }

    public Double getHealth() {
        return this.health;
    }

    public Double getShield() {
        return this.shield;
    }

    @Override
    public void getDamage(Double damage) {
        Double health = getHealth();
        Double shield = getShield();
        if(shield > 0) {
            shield -= damage;
        }
        else if (health > 0) {
            health -= damage;
        }
        else {
            health = 0d;
            shield = 0d;
        }

        if(health < 0d) {
            health = 0d;
        }
        if(shield < 0d) {
            shield = 0d;
        }
        setHealth(health);
        setShield(shield);
    }

    public void plusHealth(Double health) {
        setHealth(getHealth()+health);
    }
    public void plusShield(Double shield) {
        setShield(getShield()+shield);
    }
}
