package com.enchantme.nami.game_objects.basic;

/*
All bullets in the game should extend this Bullet class !
 */
abstract class Bullet extends MovableObject {
    Bullet(String spriteName) {
        super(spriteName);
    }
}
