package com.enchantme.nami.game_objects.basic;

import com.badlogic.gdx.math.Vector2;

/*
A
 */
public abstract class MovableObject extends GameObject {
    protected Vector2 velocity;

    public MovableObject() {
        super();
        velocity = new Vector2(0,0);
    }
    public MovableObject(String spriteName){
        super(spriteName);
        velocity = new Vector2(0,0);
    }
    public abstract void move();
}
