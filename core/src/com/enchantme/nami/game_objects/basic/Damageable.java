package com.enchantme.nami.game_objects.basic;

public interface Damageable {

    void getDamage(Double damage);

}
