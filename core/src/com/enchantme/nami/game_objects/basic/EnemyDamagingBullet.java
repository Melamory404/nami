package com.enchantme.nami.game_objects.basic;

public abstract class EnemyDamagingBullet extends DamagingBullet {

    protected boolean killed = false;
    protected boolean destroyed = false;

    public EnemyDamagingBullet(String spriteName) {
        super(spriteName);
    }

    public abstract boolean isDead();
}
