package com.enchantme.nami.game_objects.basic;

import com.badlogic.gdx.math.Vector2;

/*
All enemies that player will fight with, should extend this class !
 */
public abstract class Enemy extends MovableObject {

    protected boolean killed = false;
    protected boolean destroyed = false;

    protected Vector2 startPosition;
    protected Vector2 currentPosition;

    public Enemy(String spriteName, Vector2 startPosition){
        super(spriteName);
        this.startPosition = startPosition;
        currentPosition = this.startPosition;
    }

    public abstract boolean isDead();
    public abstract void attack(float delta);
    /*This method should return boolean value,
     which means you can delete this object from box2d world or not.
     */
    public abstract boolean allowToRemoveFromWorld();
    public Double getCollisionDamage() {
        return 0d;
    }
}
