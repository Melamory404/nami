package com.enchantme.nami.game_objects.basic;

/*
All bullets that can cause some damage should extend this class !
 */
public abstract class DamagingBullet extends Bullet {
    private Double damage;
    private DamageType damageType;

    public DamagingBullet(String spriteName) {
        super(spriteName);
    }
    protected void setDamage(Double damage){
        this.damage = damage;
    }
    protected void setDamageType(DamageType damageType) {
        this.damageType = damageType;
    }
    public Double getDamage() {
        return damage;
    }
}
