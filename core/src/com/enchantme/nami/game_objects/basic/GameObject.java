package com.enchantme.nami.game_objects.basic;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/**
 * Basic GameObject class for game, all GameObject's should extend this one.
 */
public abstract class GameObject extends Sprite implements Damageable{

  public GameObject(){}

  //@param spriteName path to supported by Libgdx
  public GameObject(String spriteName){
    super(new Texture(spriteName));
  }
  public abstract void render(SpriteBatch batch);
  public abstract void update(float dt);
  public abstract void kill();
  public abstract void define();
}
