package com.enchantme.nami.game_objects.basic;

public enum DamageType {
    Kinetic,
    Energy,
    None
}
