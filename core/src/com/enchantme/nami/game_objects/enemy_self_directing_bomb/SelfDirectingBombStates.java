package com.enchantme.nami.game_objects.enemy_self_directing_bomb;

public enum SelfDirectingBombStates {
    STAY,
    CHASE
}
