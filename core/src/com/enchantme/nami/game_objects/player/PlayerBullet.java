package com.enchantme.nami.game_objects.player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.DamagingBullet;
import com.enchantme.nami.system.Assets;

public class PlayerBullet extends DamagingBullet  {

    private Player player;
    private World world;
    private Body body;
    private Vector2 startPosition;
    private Vector2 currentPosition;
    private boolean killed = false;
    private boolean destroyed = false;

    private static final float deathTimeLimit = 1.8f;
    private float deathTime = 0.0f;

    private static final float maxVelocity = 5f;
    private static final float BULLET_RESIZE_FACTOR = 2f;

    public PlayerBullet(World world, Player player, Vector2 startPosition) {
        super(Assets.PLAYER_BULLET_SPRITE.getAsset());
        this.world = world;
        this.startPosition = startPosition;
        this.player = player;
        setDamage(player.getDamage());
        setSize(getWidth()/(Nami.PPM*BULLET_RESIZE_FACTOR), getHeight()/(Nami.PPM*BULLET_RESIZE_FACTOR));
        define();
    }

    @Override
    public void move() {
        setPosition(body.getPosition().x-getWidth()/2,body.getPosition().y-getHeight()/2);
    }

    @Override
    public void define() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(startPosition.x+getWidth()/2, startPosition.y+getHeight()/2);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(getWidth()/2,getHeight()/2);

        fixtureDef.shape = shape;
        fixtureDef.isSensor = true;
        body.setBullet(true);
        body.createFixture(fixtureDef).setUserData(this);
        body.setLinearVelocity(new Vector2(0,maxVelocity));
    }

    @Override
    public void render(SpriteBatch batch) {
        if(!killed){
            draw(batch);
        }
    }

    @Override
    public void update(float dt) {
        deathTime += dt;
        if(killed && !destroyed) {
            world.destroyBody(body);
            destroyed = true;
            player.removeBullet(this);
        }
        else {
            move();
        }
        if(deathTime >= deathTimeLimit) {
            kill();
        }
    }

    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public void getDamage(Double damage) {

    }

}
