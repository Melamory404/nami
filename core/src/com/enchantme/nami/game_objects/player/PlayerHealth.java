package com.enchantme.nami.game_objects.player;

import com.enchantme.nami.game_objects.basic.HealthSystem;

class PlayerHealth extends HealthSystem {

    private Player player;

    public PlayerHealth(Player player, Double health, Double shield) {
        super(health, shield);
        this.player = player;
    }

    @Override
    public void getDamage(Double damage) {
        super.getDamage(damage);
    }
}
