package com.enchantme.nami.game_objects.player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.enchantme.nami.Nami;
import com.enchantme.nami.game_objects.basic.DamageType;
import com.enchantme.nami.game_objects.basic.MovableObject;
import com.enchantme.nami.screens.ScreenEnum;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.system.Assets;
import com.enchantme.nami.system.NamiSystem;

import java.util.ArrayList;

public class Player extends MovableObject {

    private PlayerHealth playerHealth;
    private Body body;
    private World world;
    private Vector2 currentPosition;
    private ArrayList<Vector2> bulletSpawnPositions;

    private DelayedRemovalArray<PlayerBullet> playerBullets;
    private ArrayList<PlayerBullet> playerBulletsToRemove;

    private static final float shotDelay = 0.5f;
    private float timeToDelay = 0.0f;
    private boolean allowToShoot = true;
    private boolean shooting = false;

    private static final float maxVelocity = 0.1f;
    private static final float damping = 2f;
    private static final float PLAYER_RESIZE_FACTOR = 2.0f;

    private boolean isLeftPressed, isRightPressed,
            isUpPressed, isDownPressed, isFirePressed;
    private boolean killed = false;
    private boolean destroyed = false;

    private Double damage;
    private DamageType damageType = DamageType.Kinetic;

    public Player(World world) {
        super(Assets.PLAYER_SPRITE.getAsset());
        playerHealth = new PlayerHealth(this, 10d, 10d);
        currentPosition = new Vector2(0,0);
        this.world = world;
        this.damage = 5d;
        setSize(getWidth()/(Nami.PPM*PLAYER_RESIZE_FACTOR),
                getHeight()/(Nami.PPM*PLAYER_RESIZE_FACTOR));
        define();
        initBulletSpawnPositions();
        playerBullets = new DelayedRemovalArray<PlayerBullet>();
        playerBulletsToRemove = new ArrayList<PlayerBullet>();
    }


    //Specifies the offset of spawn position relative to the current position !
    private void initBulletSpawnPositions() {
        bulletSpawnPositions = new ArrayList<Vector2>();
        bulletSpawnPositions.add(new Vector2(0.05f*PLAYER_RESIZE_FACTOR,0.25f*PLAYER_RESIZE_FACTOR));
        bulletSpawnPositions.add(new Vector2(0.25f*PLAYER_RESIZE_FACTOR,0.25f*PLAYER_RESIZE_FACTOR));
    }

    @Override
    public void define() {

        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.set(5f,3.5f);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox((getWidth()/2),(getHeight()/2));

        fixtureDef.shape = shape;
       // fixtureDef.isSensor = true;
        body.setLinearDamping(damping);
        body.setAngularDamping(damping);
        body.createFixture(fixtureDef).setUserData(this);
    }

    @Override
    public void move() {
        currentPosition.x = body.getPosition().x - getWidth()/2;
        currentPosition.y = body.getPosition().y - getHeight()/2;
        setPosition(currentPosition.x, currentPosition.y);
    }

    public void setFirePressed(boolean isPressed) {
        isFirePressed = isPressed;
    }

    public void setLeftPressed (boolean isPressed) {
        if (isRightPressed && isPressed) {
            isRightPressed = false;
        }
        isLeftPressed = isPressed;
    }

    public void setRightPressed (boolean isPressed) {
        if (isLeftPressed && isPressed) {
            isLeftPressed = false;
        }
        isRightPressed = isPressed;
    }

    public void setUpPressed(boolean isPressed) {
        if (isDownPressed && isPressed) {
            isDownPressed = false;
        }
        isUpPressed = isPressed;
    }

    public void setDownPressed(boolean isPressed) {
        if (isUpPressed && isPressed){
            isUpPressed = false;
        }
        isDownPressed = isPressed;
    }

    @Override
    public void render(SpriteBatch batch) {
        if(!killed) {
            draw(batch);
        }

        for (PlayerBullet bullet : playerBullets) {
            bullet.render(batch);
        }
    }

    @Override
    public void update(float dt) {
        if(killed && !destroyed){
            world.destroyBody(body);
            destroyed = true;
            //TODO: Сделать нормальную функцию смерти со всеми сохранениями
            NamiSystem.getProfileManager().setCurrentSaveScore();
            ScreenManager.getInstance().showScreen(ScreenEnum.SURVIVAL_MODE_END);
        }
        else {
            input(dt);
            move();
        }
   //     checkLowSpeed();
    }

    private void input(float dt) {
        if (isLeftPressed) {
            velocity.x = -maxVelocity;
            body.applyLinearImpulse(new Vector2(velocity.x,0), body.getWorldCenter(),true);
        }
        else if (isRightPressed) {
            velocity.x = maxVelocity;
            body.applyLinearImpulse(new Vector2(velocity.x,0), body.getWorldCenter(),true);
        }
        if(isUpPressed) {
            velocity.y = maxVelocity;
            body.applyLinearImpulse(new Vector2(0,velocity.y), body.getWorldCenter(),true);
        }
        else if (isDownPressed) {
            velocity.y = -maxVelocity;
            body.applyLinearImpulse(new Vector2(0,velocity.y), body.getWorldCenter(),true);
        }

        if(isFirePressed) {
            shooting = true;
            if(allowToShoot){
                for (Vector2 spawnPosition: bulletSpawnPositions) {
                    playerBullets.add(new PlayerBullet(world,this,
                            new Vector2(getCurrentPosition().x+spawnPosition.x,getCurrentPosition().y+spawnPosition.y)));
                }
                allowToShoot = false;
            }
        }

        if(shooting) {
            timeToDelay += dt;
            if(timeToDelay >= shotDelay) {
                timeToDelay = 0.0f;
                shooting = false;
                allowToShoot = true;
            }
        }

        for (PlayerBullet bullet : playerBullets) {
            bullet.update(dt);
        }

        if(!playerBulletsToRemove.isEmpty()) {
            for (PlayerBullet bullet: playerBulletsToRemove) {
                playerBullets.removeValue(bullet,true);
            }
            playerBulletsToRemove.clear();
        }

    }

    public void removeBullet(PlayerBullet bullet) {
        playerBulletsToRemove.add(bullet);
    }
    @Override
    public void kill() {
        killed = true;
    }

    @Override
    public void getDamage(Double damage) {
        playerHealth.getDamage(damage);
        if(getHealth() <= 0d) {
            kill();
        }
    }

    public Double getDamage() {
        return damage;
    }

    public Double getHealth() {
        return playerHealth.getHealth();
    }
    public Double getShield() {
        return playerHealth.getShield();
    }

    public void plusHealth(Double health) {
        playerHealth.plusHealth(health);
    }

    public void plusShield(Double shield) {
        playerHealth.plusShield(shield);
    }

    public Vector2 getCurrentPosition() {
        return currentPosition;
    }
}
