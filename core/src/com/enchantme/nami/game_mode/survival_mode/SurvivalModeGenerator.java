package com.enchantme.nami.game_mode.survival_mode;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.enchantme.nami.game_mode.Generator;
import com.enchantme.nami.game_objects.basic.Enemy;
import com.enchantme.nami.game_objects.enemy_linear_ship.LinearShip;
import com.enchantme.nami.game_objects.enemy_mine.Mine;
import com.enchantme.nami.game_objects.enemy_ram_ship.RamShip;
import com.enchantme.nami.game_objects.environment.SurvivalModeBorder;
import com.enchantme.nami.game_objects.items.kits.HealthKit;
import com.enchantme.nami.game_objects.items.kits.Kit;
import com.enchantme.nami.game_objects.items.kits.ScoreKit;
import com.enchantme.nami.game_objects.items.kits.ShieldKit;
import com.enchantme.nami.game_objects.player.Player;
import com.enchantme.nami.screens.survival_mode.SurvivalModeGameScreen;
import com.enchantme.nami.system.FileTools;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

public class SurvivalModeGenerator implements Generator {

    private ArrayList<Enemy> enemies;
    private ArrayList<Enemy> enemiesToDestroy;
    private ArrayList<Enemy> enemiesToRemoveFromEnemiesToDestroy;

    private ArrayList<Kit> kits;
    private ArrayList<Kit> kitsToDestroy;

    private ArrayList<Vector2> linearShipSpawnPoints;
    private ArrayList<Vector2> ramShipSpawnPoints;
    private ArrayList<Vector2> mineSpawnPoints;
    private ArrayList<Vector2> kitSpawnPoints;

    private Player player;
    private World world;
    private SurvivalModeGameScreen survivalModeGameScreen;
    private SurvivalModeBorder survivalModeBorder;

    private Double currentScoreHealth;
    private Double scoreHealthLimit = 10d;

    private Random random;

    //TODO: Сделать пресетап всей игровой системы, так навсякий.
    private static final Type spawnPointsType = new TypeToken<ArrayList<Vector2>>(){}.getType();

    private float spawnLinearShipsTime = 0;
    private float spawnRamShipsTime = 0;
    private float spawnMinesTime = 0;

    public SurvivalModeGenerator(SurvivalModeGameScreen survivalModeGameScreen) {
        enemies = new ArrayList<Enemy>();
        enemiesToDestroy = new ArrayList<Enemy>();
        enemiesToRemoveFromEnemiesToDestroy = new ArrayList<Enemy>();

        kits = new ArrayList<Kit>();
        kitsToDestroy = new ArrayList<Kit>();

        this.world = survivalModeGameScreen.getWorld();
        this.survivalModeGameScreen = survivalModeGameScreen;

        player = new Player(world);
        survivalModeBorder = new SurvivalModeBorder(world);
        random = new Random();
        generateSpawnPoints();
    }

    private void generateSpawnPoints() {
        String systemDirectory = "System";
        linearShipSpawnPoints = generateLinearShipSpawnPoints(systemDirectory);
        ramShipSpawnPoints = generateRamShipSpawnPoints(systemDirectory);
        mineSpawnPoints = generateMineSpawnPoints(systemDirectory);
        kitSpawnPoints = generateKitSpawnPoints(systemDirectory);
    }

    private ArrayList<Vector2> generateLinearShipSpawnPoints(String systemDirectory) {
        String spawnPointsFilePath = systemDirectory + "/" + "LinearShipSpawnPoints.json";
        String spawnPointsJson = FileTools.readFileAsString(spawnPointsFilePath);

        return new GsonBuilder().create().fromJson(spawnPointsJson, spawnPointsType);
    }

    private ArrayList<Vector2> generateRamShipSpawnPoints(String systemDirectory) {
        String spawnPointsFilePath = systemDirectory + "/" + "RamShipSpawnPoints.json";
        String spawnPointsJson = FileTools.readFileAsString(spawnPointsFilePath);

        return new GsonBuilder().create().fromJson(spawnPointsJson, spawnPointsType);
    }

    private ArrayList<Vector2> generateMineSpawnPoints(String systemDirectory) {
        String spawnPointsFilePath = systemDirectory + "/" + "MineSpawnPoints.json";
        String spawnPointsJson = FileTools.readFileAsString(spawnPointsFilePath);

        return new GsonBuilder().create().fromJson(spawnPointsJson, spawnPointsType);
    }

    private ArrayList<Vector2> generateKitSpawnPoints(String systemDirectory) {
        String kitSpawnPointsFilePath = systemDirectory + "/" + "KitSpawnPoints.json";
        String kitSpawnPointsJson = FileTools.readFileAsString(kitSpawnPointsFilePath);

        return new GsonBuilder().create().fromJson(kitSpawnPointsJson, spawnPointsType);
    }

    private void spawnEnemies(float dt) {
        spawnLinearShips(dt,1);
        spawnRamShips(dt, 1);
        spawnMines(dt, 1);
    }

    private void spawnLinearShips(float dt, Integer numberOfInstances) {
        if(spawnLinearShipsTime > 3) {
            for (int i = 0; i < numberOfInstances; i++) {
                int randomSpawnIndex = random.nextInt(linearShipSpawnPoints.size());
                Vector2 randomVector = linearShipSpawnPoints.get(randomSpawnIndex);
                enemies.add(new RamShip(world, new Vector2(randomVector.x,randomVector.y)));
            }
            spawnLinearShipsTime = 0;
        }
        else {
            spawnLinearShipsTime += dt;
        }
    }

    private void spawnMines(float dt, Integer numberOfInstances) {
        if(spawnMinesTime > 5) {
            for (int i = 0; i < numberOfInstances; i++) {
                int randomSpawnIndex = random.nextInt(mineSpawnPoints.size());
                Vector2 randomVector = mineSpawnPoints.get(randomSpawnIndex);
                enemies.add(new Mine(world, new Vector2(randomVector.x,randomVector.y)));
            }
            spawnMinesTime = 0;
        }
        else {
            spawnMinesTime += dt;
        }
    }

    private void spawnRamShips(float dt, Integer numberOfInstances) {
        if(spawnRamShipsTime > 4) {
            for (int i = 0; i < numberOfInstances; i++) {
                int randomSpawnIndex = random.nextInt(ramShipSpawnPoints.size());
                Vector2 randomVector = ramShipSpawnPoints.get(randomSpawnIndex);
                enemies.add(new LinearShip(world, new Vector2(randomVector.x,randomVector.y)));
            }
            spawnRamShipsTime = 0;
        }
        else {
            spawnRamShipsTime += dt;
        }
    }

    private void spawnKits(float dt) {
        currentScoreHealth = survivalModeGameScreen.getScore();
        if(currentScoreHealth >= scoreHealthLimit) {
            int randomSpawnIndex = random.nextInt(kitSpawnPoints.size());
            Vector2 randomVector = kitSpawnPoints.get(randomSpawnIndex);
            int randomTypeIndex = random.nextInt(3);
            switch (randomTypeIndex) {
                case 0:
                    kits.add(new HealthKit(world, new Vector2(randomVector.x, randomVector.y)));
                    break;
                case 1:
                    kits.add(new ShieldKit(world,new Vector2(randomVector.x,randomVector.y)));
                    break;
                case 2:
                    kits.add(new ScoreKit(world,new Vector2(randomVector.x,randomVector.y)));
                    break;
            }
            scoreHealthLimit += 150d;
        }
    }

    @Override
    public void update(float dt) {

        player.update(dt);

        spawnEnemies(dt);
        spawnKits(dt);

        for (Enemy enemy : enemies) {
            enemy.update(dt);
        }
        for (Kit kit : kits) {
            kit.update(dt);
        }

        for (Kit kit : kitsToDestroy) {
            kits.remove(kit);
        }
        if(!kitsToDestroy.isEmpty()){
            kitsToDestroy.clear();
        }

        for (Enemy enemy : enemiesToDestroy) {
            if(enemy.allowToRemoveFromWorld()){
                enemies.remove(enemy);
                enemiesToRemoveFromEnemiesToDestroy.add(enemy);
            }
        }
        for (Enemy enemy: enemiesToRemoveFromEnemiesToDestroy) {
            enemiesToDestroy.remove(enemy);
        }
        if(!enemiesToRemoveFromEnemiesToDestroy.isEmpty()) {
            enemiesToRemoveFromEnemiesToDestroy.clear();
        }

    }

    @Override
    public void render(SpriteBatch batch) {
        player.render(batch);

        for (Enemy enemy : enemies) {
            enemy.render(batch);
        }

        for (Kit kit : kits) {
            kit.render(batch);
        }

       }

    public Player getPlayer() {
        return player;
    }

    public void destroyEnemy(Enemy enemy) {
        enemiesToDestroy.add(enemy);
    }

    public void destroyKit(Kit kit) {
        kitsToDestroy.add(kit);
    }

    //TODO: Здесь тоже нечисто, кидает экспешн при world.dispose();
    public void dispose() {
       // world.dispose();
    }
}
