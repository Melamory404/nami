package com.enchantme.nami.game_mode.survival_mode;

import com.badlogic.gdx.physics.box2d.*;
import com.enchantme.nami.game_mode.ScoreEnum;
import com.enchantme.nami.game_objects.basic.Enemy;
import com.enchantme.nami.game_objects.basic.EnemyDamagingBullet;
import com.enchantme.nami.game_objects.items.kits.HealthKit;
import com.enchantme.nami.game_objects.items.kits.ScoreKit;
import com.enchantme.nami.game_objects.items.kits.ShieldKit;
import com.enchantme.nami.game_objects.player.Player;
import com.enchantme.nami.game_objects.player.PlayerBullet;
import com.enchantme.nami.screens.survival_mode.SurvivalModeGameScreen;

public class SurvivalModeContactListener implements ContactListener {
    private SurvivalModeGenerator survivalModeGenerator;
    private SurvivalModeGameScreen survivalModeGameScreen;
    public SurvivalModeContactListener(SurvivalModeGenerator survivalModeGenerator, SurvivalModeGameScreen survivalModeGameScreen) {
        super();
        this.survivalModeGenerator = survivalModeGenerator;
        this.survivalModeGameScreen = survivalModeGameScreen;
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();
        if(fixtureA == null || fixtureB == null) return;
        if (fixtureA.getUserData() == null || fixtureB.getUserData() == null) return;

        if(fixtureA.getUserData() instanceof Enemy && fixtureB.getUserData() instanceof Player) {
            Enemy enemy = (Enemy)fixtureA.getUserData();
            Player player = (Player)fixtureB.getUserData();
             enemy.kill();
             player.getDamage(enemy.getCollisionDamage());
             survivalModeGameScreen.changeScoreLabel(5d, ScoreEnum.PLUS);
             if (enemy.isDead()){
                 survivalModeGenerator.destroyEnemy(enemy);
             }
             updatePlayerUI(player);
        }
        else if(fixtureB.getUserData() instanceof Enemy && fixtureA.getUserData() instanceof Player) {
            Enemy enemy = (Enemy)fixtureB.getUserData();
            Player player = (Player)fixtureA.getUserData();
            enemy.kill();
            player.getDamage(enemy.getCollisionDamage());
            survivalModeGameScreen.changeScoreLabel(5d, ScoreEnum.PLUS);
            if (enemy.isDead()){
                survivalModeGenerator.destroyEnemy(enemy);
            }
            updatePlayerUI(player);
        }
        else if(fixtureB.getUserData() instanceof Enemy && fixtureA.getUserData() instanceof PlayerBullet) {
            Enemy enemy = (Enemy)fixtureB.getUserData();
            PlayerBullet bullet = (PlayerBullet)fixtureA.getUserData();
            enemy.getDamage(bullet.getDamage());
            bullet.kill();
            survivalModeGameScreen.changeScoreLabel(5d, ScoreEnum.PLUS);
            survivalModeGenerator.getPlayer().removeBullet(bullet);
            if (enemy.isDead()){
                survivalModeGenerator.destroyEnemy(enemy);
            }
        }
        else if(fixtureA.getUserData() instanceof Enemy && fixtureB.getUserData() instanceof PlayerBullet) {
            Enemy enemy = (Enemy)fixtureA.getUserData();
            PlayerBullet bullet = (PlayerBullet)fixtureB.getUserData();
            enemy.getDamage(bullet.getDamage());
            bullet.kill();
            survivalModeGameScreen.changeScoreLabel(5d, ScoreEnum.PLUS);
            survivalModeGenerator.getPlayer().removeBullet(bullet);
            if (enemy.isDead()){
                survivalModeGenerator.destroyEnemy(enemy);
            }
        }
        else if(fixtureA.getUserData() instanceof EnemyDamagingBullet && fixtureB.getUserData() instanceof Player) {
            EnemyDamagingBullet damagingBullet = (EnemyDamagingBullet)fixtureA.getUserData();
            Player player = (Player)fixtureB.getUserData();
            player.getDamage(damagingBullet.getDamage());
            damagingBullet.kill();
            updatePlayerUI(player);
        }
        else if(fixtureB.getUserData() instanceof EnemyDamagingBullet && fixtureA.getUserData() instanceof Player){
            EnemyDamagingBullet damagingBullet = (EnemyDamagingBullet)fixtureB.getUserData();
            Player player = (Player)fixtureA.getUserData();
            player.getDamage(damagingBullet.getDamage());
            damagingBullet.kill();
            updatePlayerUI(player);
            }
        else if (fixtureA.getUserData() instanceof HealthKit && fixtureB.getUserData() instanceof Player) {
            HealthKit kit = (HealthKit)fixtureA.getUserData();
            Player player = (Player)fixtureB.getUserData();
            player.plusHealth(kit.getBonus());
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
            }
        else if (fixtureB.getUserData() instanceof HealthKit && fixtureA.getUserData() instanceof Player) {
            HealthKit kit = (HealthKit)fixtureB.getUserData();
            Player player = (Player)fixtureA.getUserData();
            player.plusHealth(kit.getBonus());
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
            }
        else if (fixtureA.getUserData() instanceof ShieldKit && fixtureB.getUserData() instanceof Player) {
            ShieldKit kit = (ShieldKit)fixtureA.getUserData();
            Player player = (Player)fixtureB.getUserData();
            player.plusShield(kit.getBonus());
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
        }
        else if (fixtureB.getUserData() instanceof ShieldKit && fixtureA.getUserData() instanceof Player) {
            ShieldKit kit = (ShieldKit)fixtureB.getUserData();
            Player player = (Player)fixtureA.getUserData();
            player.plusShield(kit.getBonus());
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
        }
        else if (fixtureA.getUserData() instanceof ScoreKit && fixtureB.getUserData() instanceof Player) {
            ScoreKit kit = (ScoreKit)fixtureA.getUserData();
            Player player = (Player)fixtureB.getUserData();
            survivalModeGameScreen.changeScoreLabel(kit.getBonus(), ScoreEnum.PLUS);
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
        }
        else if (fixtureB.getUserData() instanceof ScoreKit && fixtureA.getUserData() instanceof Player) {
            ScoreKit kit = (ScoreKit)fixtureB.getUserData();
            Player player = (Player)fixtureA.getUserData();
            survivalModeGameScreen.changeScoreLabel(kit.getBonus(), ScoreEnum.PLUS);
            kit.kill();
            survivalModeGenerator.destroyKit(kit);
            updatePlayerUI(player);
        }

    }

    private void updatePlayerUI(Player player) {
        survivalModeGameScreen.setHealthLabel(player.getHealth());
        survivalModeGameScreen.setShieldLabel(player.getShield());
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
