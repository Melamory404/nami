package com.enchantme.nami.game_mode.survival_mode;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.enchantme.nami.game_objects.player.Player;
import com.enchantme.nami.screens.GameStates;
import com.enchantme.nami.screens.ScreenManager;
import com.enchantme.nami.screens.survival_mode.SurvivalModeGameScreen;

public class SurvivalModeInputManager extends InputAdapter {

    SurvivalModeGenerator survivalModeGenerator;
    SurvivalModeGameScreen survivalModeGameScreen;

    Player player;

    public SurvivalModeInputManager(SurvivalModeGenerator survivalModeGenerator, SurvivalModeGameScreen survivalModeGameScreen) {
        this.survivalModeGenerator = survivalModeGenerator;
        this.survivalModeGameScreen = survivalModeGameScreen;
        this.player = survivalModeGenerator.getPlayer();
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.LEFT) {
            player.setLeftPressed(true);
        }
        else if (keycode == Input.Keys.RIGHT) {
            player.setRightPressed(true);
        }
        else if (keycode == Input.Keys.UP) {
            player.setUpPressed(true);
        }
        else if (keycode == Input.Keys.DOWN) {
            player.setDownPressed(true);
        }

        if(keycode == Input.Keys.SPACE) {
            player.setFirePressed(true);
        }

        if(keycode == Input.Keys.ESCAPE) {
            survivalModeGameScreen.pauseScreen();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        if (keycode == Input.Keys.LEFT) {
            player.setLeftPressed(false);
        }
        else if (keycode == Input.Keys.RIGHT) {
            player.setRightPressed(false);
        }
        else if (keycode == Input.Keys.UP) {
            player.setUpPressed(false);
        }
        else if (keycode == Input.Keys.DOWN) {
            player.setDownPressed(false);
        }

        if(keycode == Input.Keys.SPACE) {
            player.setFirePressed(false);
        }

        return false;
    }
}
