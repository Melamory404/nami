package com.enchantme.nami.game_mode;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Generator {
    void update(float dt);
    void render(SpriteBatch batch);
}
