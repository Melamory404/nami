package com.enchantme.nami.game_mode;

public enum ScoreEnum {
    PLUS,
    MINUS,
    SET,
    ERASE
}
